# Embedding 

## Contextual Meaning of words

Word has meaning especially in the context of a sentence. There may be difference words that have the same meaning when use in the same context call synonym. Also, there are words that have opposite meaning when use in the same context this is called antonym.
- word that have same meaning tend to appear in the same context. But shared context doesn't mean shared meaning

## Core concept

- Language convey core concept call lemma e.g. run is core to running,ran
- Language also have sense (meaning) base on the context e.g. I ran(did) a shop, she runs(jog) daily
- word with structural relationship share a semantic field (item that it belong to) e.g. restaurant has chef, waiter, kitchen
- word sense may be hierarchically organized e.g. bike is subordinate to verticals

## Represent words relationship

One method would be to put them on a two dimensional matrix to show their relationship in term of distance between each others

 ![](./images/embedword.png)

## Text represent as number

### Bag or word

- has sparsity problem due to Zipf's law
- ignore relationship between word
- no way to capture context  dependency


Compute the distance of word
Popular method of calculation is using cosine of similarity



$\color{orange}{cosine(\vec{v},\vec{w})=\frac{\vec{v}\bullet \vec{w}}{|\vec{v}||\vec{w}|}=\frac{\sum_{i=1}^{n} v_i w_i} {\sqrt{ \sum_{i=1}^{n} v^2} \sqrt{ \sum_{i=1}^{n} w^2} } }$

## PCA (principal component analysis)

![](./images/pca.jpg)

Convert word with context into number or vector of number.

Use to reduce the number of variables or dimensions of a data set, while preserving as much information as possible.



Principal Component Analysis, or PCA, is a dimensionality-reduction method that is often used to reduce the dimensionality of large data sets, by transforming a large set of variables into a smaller one that still contains most of the information in the large set.

Reducing the number of variables of a data set naturally comes at the expense of accuracy, but the trick in dimensionality reduction is to trade a little accuracy for simplicity. Because smaller data sets are easier to explore and visualize and make analyzing data much easier and faster for machine learning algorithms without extraneous variables to process.

PCA drawback is that is not able to retain info about term frequency that occur in the context of the document.

Its require to have orthogonal axis or 90 degree to each axis. this limitation of linear relationship of data only. 

Term-frequency inverse document frequency

It is used to overcome the limitation of PCA to compute TF-IDF using 

$\color{orange}{ W_{t,d}  = tf_{t,d} * idf_t}$ 

where

$\color{orange}{tf_{t,d} = 1 + log_{10}count(t,d) if count(t,d)>0,0\hspace{1mm} otherwise  }$
$\color{orange}{idf_i = log(\frac{N}{df_i})}$
where N = # context in doc(d), dfi = # context doc(d) that have word i



<!-- 
$\color{orange}{ \[f(n) = \begin{cases} n/2 & \quad \text{if $n$ is even} \\
-(n+1)/2  \text{if $n$ is odd}\\ \end{cases} \] }$ -->


## Word2Vec

It allows for more nuance and don't require linear relationship of data there are two approaches:
- Bag of words input group of  word to the model then predict a word that fit the group of word
- Skip grams to reverse of bag of word technique which use a word to predict which this word fit into

### One-hot encoding

A Technique to locate a word by its index which set it to 1 and all other to 0 another a lookup table of word in a vocabulary

