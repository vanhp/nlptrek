# <span style="color:lightblue"> Performance Measurement</span>

Classification metric
- Accuracy
- Precision
- Recall
- f1 score (harmonic mean of precision/recall)

Classification with probability task
- area under the precision recall curve
- area under the receiver operation characteristic curve(AUC)


One of the tool that uses  to gate the performance of a model is confusion metric.
It compares the model prediction vs the label provided
- if prediction is 1 and label is 1 ==> true positive (TP)
- if prediction is 1 and label is 0 ==> false positive (FP)
- if prediction is 0 and label is 0 ==> true negative (TN)
- if prediction is 0 and label is 1 ==> false negative (FN)

where prediction is either positive or negative
and label is either true or false


## <span style="color:forestgreen"> Accuracy calculation</span>
this is simple metric it work well if the data is 50/50 split
if data is skew toward one end it's not accurate.

$\color{orange}{ accuracy = \frac {number correct prediction}{total number of example}}$

or

$\color{orange}{ accuracy = \frac {TP + TN}{TP + FP + FN + TN}}$

## <span style="color:forestgreen"> Precision and Recall</span>
These metric perform better than accuracy when data is not balanced

### <span style="color:coral"> Precision </span>

This metric measures 
- the portion of the positive predition that is correct.
- range [0,1]

$\color{orange}{ precision = \frac {TP}{TP + FP}}$

### <span style="color:coral"> Recall</span>

this metric measure 
- the actual positive value that were predicted correctly

$\color{orange}{ precision = \frac {TP}{TP + FN}}$

## <span style="color:forestgreen"> F1 score</span>

This metric combine both precision and recall.

It's may be easier to use the average of both method this call harmonic mean. Since these metrics can be contradict each other
increase one may reduced the other.

$\color{orange}{ F1\hspace{1mm}score = 2\hspace{1mm} *\hspace{1mm}\frac {precision\hspace{1mm} *\hspace{1mm} recall}{precision \hspace{1mm} + \hspace{1mm}recall}}$

## <span style="color:forestgreen"> Metric of Probability</span>

In case where the model don't actually predict the class directly but instead output a probability the standard way to handle this is using 
- the probability value = 0.5 to decide value to be 0 < .5 or 1 > .5
or
-  custom value that appropriate 

## <span style="color:forestgreen"> Visualization</span>

AUPRC curve (precision-recall curve)
Plotting the precision(y) vs recall(x) metric on a grid help improve understanding of the performance of the model. The compute of area under the curve (AUCPRC)can provide more insight into the information
![](./images/prec-recall.jpg)


AUROC curve (receiver operating characteristic curve)
Plotting Recall(y) vs false positive rate(x) this is useful in case where false positive is important
![](./images/recall-fpr.png)

Recall and FPR are complementary metric

- Precision and recall focus on how well a model is able to predict the correct positive class

- recall and false positive rate focus on the correctness and incorrectness of positive prediction
- false positive rate focus on portion of positive prediction that is actually negative

$\color{orange}{ false positive rate = \frac {FP}{TN + FP}}$

Metric selection

- choose metric according to tradition
- choose metric according rationale that appropriated explained
