# Constituency Parsing

A method that instead of looking at language as simply a statistical point of view. But looking as it as transcend the symbolic representation of language.

Language is more complex than just sequence of token. It has rule of grammar, relevant social and physical context that is not written in the text itself e.g. my fat cat eat caviar refer to a cat, the fat cat eat caviar refer to rich guy, the fat cat serves caviar refer to a restaurant name.

In many case these meaning cannot extract out of the written text directly. It's required understanding the environment and/or time frame surround the text.


## Parsing order

In order to get the correct or  best result there should be an order of parsing to remove structure ambiguity due more than one way to parse is correctly but there is only one correct answer e.g. I shot the dog in my sleep could mean I sleep then shot the dog while sleep  or I shot the dog in my dream.

### Type of ambiguity
When it is grammatically correct but semantically incorrect.

1. Attachment allow the constituent to attach to the parse-tree at more than one place e.g. we saw the Eiffel tower flying to Paris or Eiffel flying to Paris we saw

2. coordination when different set of phrases can be conjoined by a conjunction, such as "and" e.g. old men and women or old men and women

### Disambiguate

Syntactic disambiguation via CKY algorithm require that our grammar be in Chomsky Normal form(CNF)

### CNF
Require that the non-terminal node in the parse-tree must have exactly two children or binary tree.

The grammar must be expand to either:
1. two non-terminal e.g. A -> BC 
2. a single terminal     A -> w

example: of converting the non CNF to CNF 

the infinite verb-phrase is replace with two rule
e.g. INF-VP -> to VP ==> INF-VP -> TO VP and TO -> to

or

rule with a single non-terminal on the right are eliminated by rewrite the right hand side of original rule to be the right-hand side of all the rule they ultimately lead to.
A -> B
B -> C
C -> to 
become
A -> to

### CKY algorithm

The CKY algorithm can be used to parse the sentence into all possible way to parse into a table
![](./images/cky_table.jpg)

## Statistical constituency Parsing

The CNF rule can have probability assign to them that would give extra info about a word. They are collected into a database call Treebank such as Penn Treebank.
![](./images/treebank.jpg)

### Probabilistic Context Free Grammar(PCFG)
The PCFG has two drawbacks that not suitable to model structural and lexical dependencies.
1. Independent assumption: 
    it impose independence assumption on probability that lead to poor modeling of structural dependencies. Naive assumption that word are independent from each other.
2. No lexical condition:
    don't model syntactical fact about specific words, which lead to problem of sub-categorization ambiguity and preposition attachment.
    e.g. some word highly likely to show up in certain context that other there no way find that out here.

### Probabilistic Lexicalized CFG
May solve PCFG problem but incurs complication.


## Dependency Parsing

In a sentence some word are depended link to another word in the same sentence these hidden link is call dependency grammars.
![](./images/depgram.jpg)

This dependency can be depicted as graph and must have:
1. Must have single root node (vertex) no input link
2. each node have only one input link
3. there is a unique path from root to each node
![](./images/depgram2.jpg)

This graph may be represented with adjacency matrix


### Algorithm for dependency tree

- transitional-based approach
    - shift-reduce
- Graph algorithm
    - maximum spanning tree

#### Projective algorithm
This type of algorithm are not allowed to have arrow that cross each other.

Arc-standard approach
walk across the input buffer of words from left to right 
for each word $w_i$ parser can:
    shift: put a word in the top of stack
    left-arc: assign the word on the top of stack as head of previous word in the stack
    right-arc: assign the previous word in the stack as head of the word at the top of stack.

![](./images/arcparser.jpg)
![](./images/arc2.jpg)

The state of the art of these parser that use transition-base system uses supervise machine learning to train the classifier to do parsing.

### Graph base Parsing

It search through tree to find possible tree for a given sentence that maximize the score Maximum spanning tree with the Chu-Liu Edmond algorithm.
