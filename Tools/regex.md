# <span style="color:lightblue"> Regular expression (regex)</span>

A DSL that use to processing text e.g. finding, validating, replacing,splitting text string


## <span style="color:forestgreen"> Python re package</span>

regex rule and symbol
- . any character except line break
- \ escape character 
- ^ start of string
- $ end of string
- | or
- ? option (1 or none)
- * 0 or more character/digit
- + 1 or more
- () grouping
- [] take one
- {n} take n time

example

(J|\s)a  "Jack is a boy" -> Ja a
```regex
[{"indices":m.span(),"match":m.group()} for m in re.finditer("(J|\s)a","Jack is a boy.")]

\d+(\.\d+)? "45 aa, 92.3" -> 45  92.3
[{"indices":m.span(),"match":m.group()} for m in re.finditer("\d+(\.\d+)?","45 aa, 92.3")]
```

### <span style="color:coral"> negative look ahead</span>

Looking for file name with exe extension
```regex
.*[.](?!exe$)[^.]*$
```
- if the expression is not match at the point look ahead in text
- the $ use to make sure that only start with "exe" 
- [^.]* make sure multiple "." is allowed
