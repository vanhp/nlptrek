# Neural Network

A process of stacking many simple functions together to create a complex network.


$\color{orange}{\hat y = \sigma(w_0 + w_1\sigma_1(x)+w_2\sigma_2(x)+ ...+ )  }$

### Activate functions

The activate function is used to compress the value into the range of interest usually 0 and 1
It also act as non-linear component in a system that otherwise linear. This provide the network to ability to expand by stacking even more layers to increase the capability.

<!-- {{<embed simpleNN.ipynb sigmoid_plot >}} -->

#### Node

Each combination of activate function and weight is called a node. 
Each node is connected to all of its immediate neighbor node to form a layer. 
Each layer i connected to previous and next layer to form a network. There is no limit to size of of the layer or number of layer. Only in practice where computing resource is limit that which in turn limit the size of the network.

### Nomenclature
- input layer the first layer that feed input data to other layer
- hidden layer any layer after input layer or middle layer
- output layer the last layer that output the prediction
- full connected network all the layer are connected to each other
- feet forward is where the connection data flow forward from input layer to output layer
- bias is the constant per node a b the intercept in linear equation
- weight is the computed input data
- input(x) a data that come from data set that feed input the network

## Gradient Descent 

Play a critical roll in helping the network "learn" or the ability to adjust itself by reduce the loss as each process of input data.
![](./images/gradient2.png)


