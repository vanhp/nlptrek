# Transformers


RNN intention is to pass the context of prior event to decoder unit but the context that were pass on is focus on the latest context while the early event has less influence due to the averaging mechanism.

## Self-attention
![](./images/attention.jpg)
So the Attention mechanism was developed in order to pass context of prior event that are relevance to the current decoder task. So there no need to use RNN mechanism anymore since the Attention mechanism is already do that with even more precise info that are relevance to the decoder step. 
The self-attention method is to provided context to all units include both the encoder and decoder sections1. 
![](./images/self_attn.jpg)
## Multi-Head attention
this mechanism can be expanded by copying them into  multiple units this is call multiple attention-heads. This allow it to have many contexts vector which contain nuance info and can be combined together. This technique is simpler and more flexible than RNN encoder-decoder network. 

But it has draw back that it doesn't capture the order of the input sequence

![](./images/multiheadAttn.png)

## Tranformer

![](./images/transformer.jpg)
In order to capture the position of the input sequence a new technique is developed by using the sinusoidal wave technique and to use vector of function to encode the position. Since the sin-wave can record the the exact position and relative position of each input token. It can be used as a look up table for each of token position

$\color{orange}{ PE_{(pos,2i)} = sin(\frac{pos}{10000\frac{2i}{d_{model}}}) }$

$\color{orange}{ PE_{(pos,2i+1)} = cos(\frac{pos}{10000\frac{2i}{d_{model}}}) }$

- where pos is  word position
- embed dimension is i
- number of embedding dimension is d_model

![](./images/pos_enc.jpg)

The sin wave capture time step position and multiple context from multi-head attention without RNN is call transformer.