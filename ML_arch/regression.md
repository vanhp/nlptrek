# <span style="color:lightblue"> Regression</span>

A technique that try to explain the relationship between the input and output has a linear relation to each other  can be approximate with a straight line e.g. find the line that cover most of the point

Machine learning is a technique that use the property of the data to solve for the most optimal value of the parameter e.g. find the best slope of line m and intercept b as in

 $\color{orange}{y = mx+b}$

## <span style="color:forestgreen"> Logistic regression</span>

A technique to assign a probability to binary outcome.

In Linear regression the algorithm try to find parameter that minimized the error that arise from predicted output and the ground truth value that come from the provided label of the input data. For example one of the error that often used is the mean square (remove negative value so it won't cancel similar positive value) error or 

### <span style="color:coral">L2 loss</span>

$\color{orange}{MSE = \frac{\overset{n} {\underset{i=1}\sum}(y_i - y_i^p)^2 }{n} }$

or mean absolute error or

### <span style="color:coral">L1 loss</span>

$\color{orange}{MAE = \frac{\overset{n} {\underset{i=1}\sum}(|y_i - y_i^p|) }{n} }$

L2 is more sensitive to the outlier value than L1

In real world data tend to not conform to straight line that go from 0 to $\infty$ a new line such as sigmoid is better approximation. It range from 0 to 1 but asymptotic toward 1 or 0


$\color{orange}{\hat y = \frac{1}{1 + e ^{-(m * x + b)} } }$

when taking exponent of m will give an odd-ratio; that is multiple increase of $\hat y$ when increase x by 1 unit

### <span style="color:coral">cross-entropy loss</span>

An error function that uses with linear regression model since this model simply predict a single data point whether the output will be 1 or 0.

$\color{orange}{p(y|x) = \hat y^y(1- \hat y)^{1-y} }$

for a set of data points use this equation

$\color{orange}{ \underset {\theta} \argmax \overset{n}{ \underset{i=1}\sum} log P(y^{(i)} | x^{(i)})  }$

### <span style="color:coral">Warning numerical underflow</span>

when multiplying lot of small value it may be underflow so taking log on both side of the equation to avoid underflow problem.

$\color{orange}{log\hspace{1 mm} p(y|x) = y log\hspace{1mm} \hat y + (1- y)\hspace{1mm}log(1- \hat y) }$

Cross entropy loss
It's simply multiply it by -1

$\color{orange}{-log\hspace{1 mm} p(y|x) = -[y log\hspace{1mm} \hat y + (1- y)\hspace{1mm}log(1- \hat y) ]}$

Issue with logic regression

When there are large features in the data this can result in 
- overfitting or 
- confusing 

to model when there are many colinear it data set e.g. two word that always occur together.

## <span style="color:forestgreen"> Regularization</span>

to reduce this problem use regularization by add a term into the equation to provide additional penalty for large coefficients.

$\color{orange}{ \hat \theta =  \underset {\theta} \argmax \overset{n}{ \underset{i=1}\sum} log P(y^{(i)} | x^{(i)}) - \alpha R(\theta)  }$

There two types of regularization

### <span style="color:coral">L2 regularization </span>
- this method provide quadratic function of the weight

$\color{orange}{ R(\theta) = \overset{n}{ \underset{j=1}\sum} \theta{_j}^{2} = ||\theta ||^{2}_{2} }$

### <span style="color:coral">L1 regularization a linear function of weight</span>

$\color{orange}{ R(\theta) = \overset{n}{ \underset{j=1}\sum} |\theta_i| = ||\theta ||_1 }$

### <span style="color:coral">Handling more than two class of output</span>

Using the multinormial logistic regression to tackle this problem and use softmax a generalization version of sigmoid function 

$\color{orange}{ softmax(Z_i)= \frac{e^{z_i}}{ \overset{k}{ \underset{j=1}\sum} e^{z_j}} \hspace{1mm} 1 \leq i \leq k }$

softmax will return a vector of probability one for each class

### <span style="color:coral">Handling ordered in the class</span>

when the class has ordered the ordinal logistic regression is needed since it's design to work with orderly structure data and will return vector with order

This is done by divide the data into subgroup then create and train a simple model(binary classification) for each subgroup finally take the difference or combine the result from each model to answer the question.