# Encoder-Decoder Network

## Text translation from english (X) to spanish (y)
For sequence to sequence model the process is convert the text sentence e.g. "the dog eat cheese" X into one-hot encoder then put the index location of the word into H vector as one-hot vector. Then do matrix dot product with the embedding matrix E which give

$\color{orange}{ X = E*H}$

given english text X that would translate into spanish y there are W U V matrix that would help map the translation.
W is the matrix that has the relationship between the input X and node in the next layer
U is the matrix that has the info of the previous state that is fed into the current state of the node
V is the matrix that has the new info to be added to the current state after multiplication will generate output y
![](./images/seq2seq.jpg)

$\color{orange}{ h^{(t)} = σ (Uh^{(t-1)}+  Wx^{(t)})  }$

$\color{orange}{ y^{(t)} = σ (Vh^{(t)})  }$
