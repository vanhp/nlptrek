
# <span style="color:lightblue"> Naive Bayes theorem</span>

A theorem that try help on understanding the relationship of probabily of event happen in one direction or more direction

## <span style="color:forestgreen"> Probability </span>

Describing the likelyhood of an event will occur in the circumstance.
using the formula 

 $\color{orange}{P(A \& B) = P(A) * P(B|A) }$

 and 

  $\color{orange}{P(B \& A) = P(B) * P(A|B) }$

example:

 $\color{orange}{P(A = 'the'\hspace{1mm} \& \hspace{1mm}B = 'if') = P(A='the') * P(B='cat') = 60\% * 40 \% = 24\%}$

or combine them together

 $\color{orange}{P(A \& B) = P(B\&A) }$
 $\color{orange}{P(B \& A) = P(B) * P(A|B) =  P(A \& B) = P(A) * P(B|A)}$ 

 Bayes Rule

 $\color{orange}{P(A|B) = \frac{likelihood\hspace{1mm} * \hspace{1mm}prior}{evidence} }$

  $\color{orange}{P(A|B) = \frac{P(B|A) * P(A)} {P(B)}}$

 This can be used for computing difficult to measure things by using easy to collect things
and use it to compute prior value such as P(C) which can be found in training data or simply assign a value to it.n


where C is happy word and w the word index in the sentence.

$\color{orange}{P(C|w)\hspace{1mm} \alpha \hspace{1mm}(C)* P(w|C)}$

To simplify the computation simple do of simple assumtion.