# $\color{orange}{ Classification}$

Process of assigning thing into group according to shared qualification or characteristic.


All classification involve create decision boundary that separate a group into
multiple groups. These groups share certain characteristic that are common among group.

## <span style="color:forestgreen"> Type of groupings</span>

- Binary 
    - separate into 2 groups e.g. yes, no, big,small...
- Multinormial
    - assign into one of n groups e.g. high,medium,low, very low
- Ordinal
    - ordered of n groups e.g. baby,teen,elder
- Multiclass
    - subgroup of n group k or n groups e.g. (big,high),(clean,square,blue)

### <span style="color:coral"> Traditional ML </span>
The tradition ML tend to manipulate data to make it work with mode by apply various technique of refining.

### <span style="color:coral">  Contemporary ML </span>
tend to let the model learn directly from data with no manipulation

### <span style="color:coral"> Supervised learning</span>
- when a label or guidance is provided with the data to gate the performance of the model

### <span style="color:coral"> Unsupervised learning</span>
- when there are no label or instruct is provide to the model and let the model learn from the data directly
