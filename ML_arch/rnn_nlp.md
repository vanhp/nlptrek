# Recurrent Neural Networks

The Markov model weakness is that it's limit to context window(scope) that was chosen.

## Recurrent NN
Using the info that was used to predict prior data then feed that info to model while it was considering current tag.

## RNN application
- POS
- NER
- De-identification
- Translation
- sequence-to-sequence
- chatbot
- question-answer
- sequence classification
- sentiment

![](./images/rnn.jpg)

## Bidirectional RNN
A technique to train two independent RNN where one process from start to end the other process from end to start then combine the output from both into single one

![](./images/birnn.jpg)


## Long short term memory (LSTM)
![](./images/rnn-lstm.jpg)
An RNN that has the capability to forget the info that is not relevant to the current task.

![](./images/lstm.jpg)

### LSTM has 
- forget gate to delete info of non relevant from current context
- add gate to select new info into current context with tanh activation that indicate the direction of info(should care about) and a sigmoid to indicate the scaling(how much should be care about) factor of the info to be add to forget gate to produce state context
- out gate with sigmoid combine with state context to output result