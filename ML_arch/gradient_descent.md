
# Gradient decent

When plotting the loss function and the parameter values with frequent enough data point show the gradual transition from the high loss down to lowest loss value. A process that try to find the lowest loss or optimal value by step by step moving downward toward lowest point is call gradient descent.

![](./images/gradientdescent.jpg)

### Cross entropy loss

$\color{orange}{log\hspace{1 mm} p(y|x) = y log\hspace{1mm} \hat y + (1- y)\hspace{1mm}log(1- \hat y) }$


### Logistic regression

$\color{orange}{\hat y = \frac{1}{1 + e ^{-(m * x + b)} } }$

## Compute the gradient

Using the following equation to compute the each gradient step for both m and b.


$\color{orange}{\frac{\partial{loss}}{\partial{m}} = y[x(\hat y -1)] + (1 - y)[x\hat y]  }$

$\color{orange}{\frac{\partial{loss}}{\partial{b}} = y[\hat y -1] + (1 - y)[\hat y]  }$
