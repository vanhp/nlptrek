# Attention

Problem with encoder-decoder network is that it focus more the closer event than the earlier event, so it's kind of "have short memory". When it's time to decode the decoder have only info about the recent event. 

![](./images/encdc_context.jpg)

What the decoder need are the info about the all the event that happen prior so it can decide which of the prior info are relevant with the current context to be decoded. 

What is needed is a way to pass all the prior event to the decoder when it decoding.
By specify the matrix that contain event from prior state that may be relevant to current decoder state when it decoding. This matrix has remove the averaging nature of prior event.

## Attention Mechanism

$\color{orange}{c^{(t)} = ∑ a^{(t,f)} h^{(f)}_e }$

where $c$ is the context mechanism to be pass on to the decoder
- t is time step
- f is activation function

$\color{orange}{ relevance(h^{(t-1)}_d,h^{(f)}_e ) = h^{(t-1)}_d Ah_e^{(f)}   }$

$\color{orange}{ α ^{(t,f)} = softmax(relevance(h^{(t-1)}_d),h^{(f)}_e)  }$

where the $\alpha$ is the normalized (to 1) relevancy via softmax
