# Speech

Speech can provide info other than text can provide.

## Phonetics

A way to communicate either by symbol or sounds using standardize way of mapping sound to symbol. But it not perfect there some sound can not express in text or there some symbol that have similar sound or no sound at all.

The International Phonetic Alphabet (IPA) was developed to try to map all sound in human language to symbol. 

The ARPAbet is developed to work with English for example.

### Basic Sound component 

Phone: a basic unit of sound that is independent of language

Phoneme: a unit of sound that distinguish one word from another

Morpheme: basic unit of meaning that is a word or subset of word

Grapheme: basic unit of symbol or writing
![](./images/unitsound.jpg)

### Prosody

Change in rhythm and tone to convey higher order of meaning. Pitch, loudness are perceptual correlates of vocal fold vibration and signal of power.

duration, pitch, and energy convey the meaning e.g. emphasis, emotion, or question or using pitch to distinguish meaning of words.

Pitch:
the perceptual correlation of the fundamental frequency(F0) the frequency vocal vibration.

Loudness is power or energy use to produce sound it's square of amplitude

Speech signal
Sound was carry by the air and reach the ear and make it vibrate according to signal.

To convert sound from analog to digital to keep all the nuance of the sound it must be sampling as Nyquist rate that twice the frequency of the highest frequency of that sound. 
![](./images/soundimg.jpg)

To convert the digital form of the sound back to analog time domain use the discrete Fourier transform
![](./images/dtft.jpg)

Viewing the sound

The sound can be view by convert it into image using the spectrogram which take the input from Discrete Fourier transform and convert it into graph call spectrogram.
![](./images/spectogram.jpg)

when generating a spectrogram the three most important parameter are:
1. DFT size: control frequency resolution
2. Window function: control edge smoothing to reduce noise (Hann)
3. Overlap amount: control number of samples that overlap to compensate for smoothing of the Hann windowing function usually 30%-50% overlapping is good

### Adjusting Spectrogram

To match how human ear perceive sound these value must be adjust:
1. Amplitude: using logarithmic scale
2. Frequency: use mel scale to adjust frequency since not all frequency sensitive the ear

Speech can be represented as matrix of number. And can be process using NN.

It has two important characteristics
1. Temporal component include:
    energy of the signal, zero crossing rate, max/min amplitude
2. Spectral feature include:
    fundamental frequency, component frequency, spectral centroid, spectral flux, spectral density, roll-off ... which can be used to identify the pitch,note,rhythm and melody.


Note: the book for this is "speech and language processing by Dan Jurafsky and James H.Martin"  at [](https://web.stanford.edu/~jurafsky)