# Context Free Grammar (CFG)

A notation or a set of rule or syntax that express the ways of tokens of a language can be grouped, ordered, and classify. Or
provide grammar for the language .e.g. noun, verb, pronoun,determiner/article(the,a,this,that),adjective,proper-noun,proposition,conjugation ... of the language.

The symbol that correspond to words is called terminal symbols.
The set of rule that govern the terminal symbols is called lexicon

Rule can be added as needed unlimited e.g. defining a Noun Phrase/sentence(NP) that comprise with a nominal which is one or more nouns and determiner
example:

 $ Nominal → Noun | Nominal\hspace{1mm} Nouns$

A non-terminal(variable) is any symbol that is not a terminal

The context free grammar can be used to parse text or generate a new text. Which result in tree of token.

For example the fat cat ate chicken -> $[NP[_{art} the][_{nominal} [  _{noun} man][ _{verb} bites]]] [_{art} the][_{noun} dog]$


## Parse Tree
![](./images/parsetree.jpb)

The bracket [] is called a parse-tree which describe the final outcome of the rule

![](./images/pt_st.jpg)

A CFG grammar rule for english

![](./images/cfg_eng.jpg)


### Application
CFG taking advantage of the structure of the language to quickly analyze the text.

A parse tree is useful for algorithm that analyzing sentence when there is no support data e.g. grammar checking app.
It's uses mostly as a semantic analysis (derive meaning from text)for a language e.g. extracting meaning from incomplete sentence that are imperative in nature (order) by analyses the the language structure which if using statistical method may required large amount of data to analyses the sentence e.g. show the last movies, give me yesterday's news, list all flights.

Or extracting the all yes-no question which start with auxiliary verb follow by subject noun phrase follow by verb noun phrase e.g.
did you try resetting the machine?, can you tell me what day is this?

extracting open-end question which start with wh-phrase and contain noun phrase follow by verb phrase where 1st word will be who,what,where, when,how...
e.g. what language is best for new coders?, what is the point of github?