# $\color{lightblue}{ Converting\hspace{1mm} text \hspace{1mm} to \hspace{1mm} number}$

In order for computer to process text it must be converted to number since the computer only know how to process number.


## $\color{forestgreen}{  Bag-of-words\hspace{1mm}  method}$

A simple way to encode text as number is by counting the n-gram (word) that are present in the text and increment its present
 if there are more than once or set it as 0 if none are present

### $\color{coral}{ The\hspace{1mm}  drawback\hspace{1mm}  of\hspace{1mm}  this\hspace{1mm}  method\hspace{1mm}  is\hspace{1mm} \hspace{1mm}  order\hspace{1mm}  is \hspace{1mm} not \hspace{1mm} preserved.}$

  