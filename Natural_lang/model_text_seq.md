# Sequential meaning of word

Idea is to assign numerical value to word that have the similar meaning closer together and word that have opposite or different meaning as far as possible. One the word were embedded then these values can gather into matrices of similar,differences and mix meaning of different size matrices. Also, when their orders are different in the matrix it have different meaning.

One drawback is that if the matrices has different size a word in one matrix may have a different meaning in another matrix

## Markov model

It show the probability of sequences of random variables or state. Markov model can be used to compute the probability of another word might occur in a sentence.

P(a,cat,dog) =  p(a) * p(cat|a) * p(dog|a,cat)
![](./images/markov.jpg)

### Hidden Markov model

Can be used to compute temporal relationship between hidden state element and how those hidden state emit observation data in real world.
![](./images/hmm.jpg)

## Decoding

A task to find the most probable sequence of hidden states from the observation sequence.
![](./images/decoding-hmm.jpg)
a classic decoding algorithm for HMM is Vitebsk algorithm
or beam search algorithm

### Maximum entropy Markov Model

Technique to increase the like of guessing by adding new feature to the model to guess the word. Even include the Markov assumption by feeding the most likely tag of the model from previous element into the sequence as a feature.

This approach is called Maximum Entropy Markov Model (MEMM)with greedy sequence decoding. It can be improved with bidirectionally with info about future tag.
