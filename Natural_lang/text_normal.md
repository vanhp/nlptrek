# <span style="color:lightblue"> Text nomalization</span>

A process where input text must be in the form that it can be used by the model.

- The text must be tokenize it into word e.g. 'the','US','is'...
- convert to the specify format e.g. 'US' is qual to ==> 'U' '.' 'S' '.' 'A' '.' and represent equals in the data
- segment the sentence split the text into sentence e.g. identify which '.' use to end the sentence which one is not.


## <span style="color:forestgreen"> Tokenization</span>

There are some techniques use to tokenize the text. 
1. Rule base such as using regular expression to tokenize the text
2. Using data driven without writing regex code

### <span style="color:coral">  1. Rule base</span>
Split text with whitespace by:
- replace punctuation with space
- replace special character with space
- split the text at space

This method is simple but may remove meaning

#### <span style="color:salmon"> # Penn Treebank tokenization method</span>
- commonly use 
- release by LDC organization
- keep all the punctuation 
- may be difficult to implement and also remove special character


### <span style="color:coral">  2. Data-driven</span>

Byte-pair-encoding
- using text compression
- simplify text by merging frequent pairs of character into the vocabulary
