
# <span style="color:lightblue"> N-Grams </span>

A contiguous sequence of n items from a text or speech. Phonemes, syllable,letters, words. There are bi-gram,tri-gram... e.g. An, is a,

A skip-gram is a non-contiguous sequence of n item from a text or speech e.g. "An apple a day keep doctor away" -> An a


N-gram is using statistic probability of the language that was embedded to perform meaningful task is field such as speech recognition, grammar correction, translation...

## <span style="color:forestgreen"> Using N-gram model</span>

the n-gram model has side-effect as if the longer gram is used mean less matching, and shorter gram mean more match without context info.
Popular gram length is around 3-5 grams may also use resolution e.g. character-level and word-level gram together to improve performance


## <span style="color:forestgreen"> Model evaluation</span>

### <span style="color:coral">Extrinsic evaluation</span>

- measure change in performance of the model in real-world application
    - is user use the recommendation
    - do user follow the instruction

### <span style="color:coral">Intrinsic evaluation</span>

- measure model performance independent of usage
- need separate training, validation,test data
- performance of model on test data

### <span style="color:coral">Performance metric perplexity</span>
- commonly use in language model evaluation 
- the inverse probability of test set that normalized by number of words.
    - lower perplexity is better
    - mean less option for model to choose


### <span style="color:coral">Generalization and Smoothing</span>

Use to correcting sparsity, unknown words, general use of the model

#### <span style="color:salmon">dealing with unknown words</span>
- smoothing by assign some character to text in order to avoid probability of 
a word or sentence is evaluated to 0
- laplace smoothing method assuming the missing word only happen once by assign a 1 
- back-off reduce the size of n-gram until a match occur
- Kneser-Key smoothing  adjust the important of infrequent unigram in the model base difference context it appear. if is rare and tend to co-occur with lots of other words it should be preferred over the one that only pair with one other term