# <span style="color:lightblue"> language </span>

a set of convention symbol that species share that use to represent mental state

- word: a specific symbol to represent physical/metal phenomenon that other human can perform statistical or logical reasoning about the potential reality.

- Syn-set: a set of one or more synonyms that interchangeable without changing the truth value e.g. sing,serenade,chant

- corpora/corpus a large structured text

## <span style="color:forestgreen"> Property of language </span>
- stop word: a task-dependent symbol that provide no meaning to the context but use to signal start/stop sentence e.g. hmmm

- punctuation: a symbol use to clarify the meaning of other symbol in the sentence e.g. !,?...

- Common word: a symbol that most frequency use in a language signify the potential understanding of the language e.g. the,of,and,to,a..

    $\color{orange}{f(r) α \frac{1}{r^\alpha}} $
    

the common word inversely proportion to it's rank
most words are used rarely, small set of words are used most frequently, to understand the language one needs to only know a subset of the most frequent use words.

## <span style="color:forestgreen"> Analysis </span>

all language used for similar purposes
Analysis of most common words uses
- noun for time
- verb for be
- adjective for good

- non-linear,
- and high-dimension, 
- order is important,  
- dynamic (context dependent) e.g. time-dependent, location-dependent e.g. football in UK and US.



## <span style="color:forestgreen"> Language model </span>

A language model is any object that assign probability to sequence of words or speech